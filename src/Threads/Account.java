package Threads;

public class Account {
    private int balance;

    public Account() {
        this.balance = 100;
    }

    public int getBalance() {
        return this.balance;
    }

    public synchronized void withdraw(int amount) throws InterruptedException {
        while(this.balance < 50) {
            this.wait();
        }
        this.balance -= amount;
    }

    public synchronized void deposit(int amount) {
        this.balance += amount;
        this.notify();
    }
}
