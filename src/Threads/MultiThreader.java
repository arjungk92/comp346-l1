package Threads;

public class MultiThreader extends Thread{
    public int id;
    public Account account;

    public MultiThreader(int id, Account acc) {
        this.id = id;
        this.account = acc;
    }

    public void run() {
        if(this.id == 1) {
            //Perform deposit operation.
            for(int i = 0; i < 1000000; i++) {
                account.deposit(50);
            }
        }
        else {
            //Perform the withdraw operation.
            for(int i = 0; i < 1000000; i++) {
                try {
                    account.withdraw(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
