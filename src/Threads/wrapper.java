package Threads;

public class wrapper {

    public static void main(String[] args) throws InterruptedException {

        Account account = new Account();

        System.out.println("Initial Balance: " + account.getBalance());

        MultiThreader m1 = new MultiThreader(1, account);
        MultiThreader m2 = new MultiThreader(2, account);


        m1.start();
        m2.start();

        m1.join();
        m2.join();

        System.out.println("Final Balance: " + account.getBalance());
    }
}
